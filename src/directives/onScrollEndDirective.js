export default function onScrollEndDirective() {
    return {
        restrict: "A",
        link: function (scope, element, attrs) {
            // Высота видимой части
            var visibleHeight = $(window.top).height();
            // Расстояние до конца видимой части страницы, при котором будет вызываться метод
            var threshold = 1;
            // Запоминаем старое значение, чтобы определять направление скролла
            var oldAboveVisibleHeight = 0;
            $(window).scroll(function () {
                // Высота над видимой частью
                var aboveVisibleHeight = $(window).scrollTop();
                // Вызываем метод, если скроллим вниз и почти доскроллили до конца страницы
                if (aboveVisibleHeight > oldAboveVisibleHeight && $(document).height() - (visibleHeight + aboveVisibleHeight) < threshold) {
                    scope.$apply(attrs.onScrollEnd);
                    oldAboveVisibleHeight = aboveVisibleHeight;
                }
            });
        }
    };
}