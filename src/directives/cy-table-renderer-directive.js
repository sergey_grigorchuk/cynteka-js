/**
 * Created by alexander on 10.11.17.
 */

// import {generateTableTemplate} from './../templates/formTemplates'

class CyTableRendererDirective {
    /*@ngInject*/
    constructor($compile, $filter) {
        this.restrict = 'E';
        this.scope = {
            config: '=',

            getdata: '=',
            type: '@',
            processData: '=',

            filter: '=',
            filterFunction: '=',

            editFunction: '=',
            deleteFunction: '=',
            deleteAllFunction: '=',

            limit: '=',

            util: '='
        };

        this.$compile = $compile;
        this.$filter = $filter;
        this.template = require('./cy-table-renderer-template.html');
    }

    link(scope, element) {

        // Если флаг использования bootstrap-классов на теге table таблицы реестра не установлен, то по умолчанию
        // ставим его в true, чтобы все существующие реестры работали как и раньше.
        if (scope.config.bootstrapTableClass === undefined) {
            scope.config.bootstrapTableClass = true;
        }

        scope.currentDataLength = scope.limit;
        scope.currentPage = 0;

        // Элементы реестра.
        scope.data = [];

        // Выделенные элементы реестра.
        scope.selectedElements = [];

        // Все элементы реестра выделены?
        scope.allSelected = false;

        scope.sortBy = "id";
        scope.sortByOrder = "desc";
        //Функция проверки как рисуется шаблон(функция или html)
        scope.isTemplate = function (renderer) {
            return typeof renderer === 'string';
        };

        scope.needToBeShown = function (renderer) {
            return typeof renderer !== 'undefined';
        };

        //Функция для возможности отображения к полям объекта как: object["field1.field2"]
        scope.resolve = function (path, obj) {
            return path.split('.').reduce(function (prev, curr) {
                return prev ? prev[curr] : null
            }, obj || self)
        };

        updateTableData();

        scope.$on('updateTableRenderer', function (e) {
            scope.currentPage = 0;
            scope.data = [];
            updateTableData();
        });

        scope.loadMore = function () {
            scope.currentPage++;
            updateTableData();
        };

        var previousField = 'id';
        scope.order = function (field) {
            if (field) {
                scope.sortBy = field;
                // Порядок сортировки меняется в том случае, если на одно и то же поле жмякают дважды, иначе остаётся старым
                scope.sortByOrder = field === previousField ? (scope.sortByOrder == 'desc' ? 'asc' : 'desc') : scope.sortByOrder;
                previousField = field;
                scope.currentPage = 0;
                scope.data = [];
                updateTableData();
            }
        };

        scope.delete = function (object) {
            scope.deleteFunction(scope, object);
        };

        scope.deleteAll = function () {
            scope.deleteAllFunction(scope.data);
        };

        scope.edit = function (object) {
            scope.editFunction(object);
        };

        scope.loadMoreIsVisible = function () {
            return scope.currentDataLength % scope.limit == 0;
        };

        // Выделение элемента / снятие выделения с элемента.
        scope.select = function(element) {

            // Меняем выделение элемента.
            element.selected = !element.selected;

            // Ищем его в массиве выделенных элементов.
            if (!scope.selectedElements) {
                scope.selectedElements = [];
            }
            var elementIndex = scope.selectedElements.indexOf(element);

            // Добавляем элемент в выделенные.
            if (element.selected) {

                if (elementIndex === -1) {

                    scope.selectedElements.push(element);
                }
            }
            // Удаляем элемент из выделенных.
            else {

                if (elementIndex !== -1) {

                    scope.selectedElements.splice(elementIndex, 1);
                }
            }

            // Все элементы реестра выделены?
            scope.allSelected = scope.selectedElements.length === scope.data.length;

            // Уведомляем, что выделение изменилось.
            scope.$emit('selectionchange', scope.selectedElements);
        };

        // Выделение всех элементов / снятие выделения со всеъ элементов.
        scope.selectAll = function() {

            // Снимаем выделение со всех.
            if (scope.allSelected) {

                scope.data.forEach(element => {

                    scope.select(element);
            });
            }
            // Выделяем те, что ещё не выделены.
            else {

                scope.data.forEach(element => {

                    if (!element.selected) {

                    scope.select(element);
                }
            });
            }
        };

        // Делает запрос на сервер и обновляет содержимое таблицы
        function updateTableData() {
            Promise.resolve(scope.getdata(getQueryParameters())).then(result => {
                var processedData = scope.resolve(scope.type, result.data);
                if (scope.processData) {
                    processedData = scope.processData(processedData);
                }
                scope.data = scope.data.concat(processedData);
                scope.currentDataLength = scope.data.length;

                if (!scope.$$phase && !scope.$root.$$phase) {
                    scope.$apply();
                }
            });
        }

        // Возвращает объект с параметрами для запроса на сервер
        function getQueryParameters() {
            return {
                params: scope.filter,
                sortBy: scope.sortBy,
                sortByOrder: scope.sortByOrder,
                page: scope.currentPage,
                limit: scope.limit
            };
        }

    }

    static directiveFactory($compile, $filter) {
        CyTableRendererDirective.instance = new CyTableRendererDirective($compile, $filter);
        return CyTableRendererDirective.instance;
    }
}

CyTableRendererDirective.directiveFactory.$inject = ['$compile', '$filter'];

export {CyTableRendererDirective}