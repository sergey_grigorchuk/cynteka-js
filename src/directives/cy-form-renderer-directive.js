/**
 * Created by alexander on 09.11.17.
 */

import {generateFormTemplate} from './../templates/formTemplates';
import {formRowConfig} from './../config/configGenerator';

class CyFormRendererDirective {
    /*@ngInject*/
    constructor($compile, $http) {
        this.restrict = 'E';
        this.scope = {
            config: '=',

            editedObject: '=',

            formName: '@',
            formModelName: '@',

            submitButtonText: '@',
            submitFunction: '=',
            submitFunctionName: '@',

            updateButtonText: '@',
            updateFunction: '=',
            updateFunctionName: '@',

            filterFunction: '=',

            // Показать или скрыть кнопки Фильтровать и Очистить. Показываем, если не указано
            showFilterButtons: '@'
        };

        this.$compile = $compile;
        this.$http = $http;
    }

    link(scope, element) {
        var submitFunction = scope.submitFunctionName;
        var submitButtonText = scope.submitButtonText;
        if (scope.editedObject != null) {
            scope[scope.formModelName] = scope.editedObject;
            submitFunction = scope.updateFunctionName;
            submitButtonText = scope.updateButtonText;
        } else {
            scope[scope.formModelName] = {};
        }
        var showFilterButtons = typeof scope.showFilterButtons == 'undefined' ? true : (scope.showFilterButtons == 'true');
        scope.rowConfig = formRowConfig(scope.config);
        var template = generateFormTemplate(scope.config, scope.formName, scope.formModelName, submitFunction, submitButtonText, scope.rowConfig, showFilterButtons);
        element.replaceWith(this.$compile(template)(scope));
        // configPostprocessor(scope.config);
        
        //Функция для возможности отображения к полям объекта как: object["field1.field2"]
        var resolve = function(path, obj) {
            return path.split('.').reduce(function(prev, curr) {
                return prev ? prev[curr] : null
            }, obj || self)
        };

        scope.submit = function () {
            scope.submitFunction(scope.config, scope[scope.formModelName]);
        };

        scope.update = function () {
            scope.updateFunction(scope.config, scope[scope.formModelName]);
        };

        scope.filter = function () {
            scope.filterFunction(scope[scope.formModelName]);
        };

        scope.clear = function () {
            scope[scope.formModelName] = {};
            scope.filter();
        };
        
        var http = this.$http;
        scope.uiSelectSearch = function ($select, ajaxUrl, formField, dataPath) {
            return http.get(ajaxUrl, {
                params: {
                    q: $select.search,
                    size: 10,
                    projection: "refbook"
                }
            }).then(function (response) {
                scope["uiSelectAjaxData_" + formField] = resolve(dataPath, response.data);
            });
        }

        // Скрывает все строки, расположенные ниже данной - нужно для скрытия расширенного фильтра
        scope.changeLowerLevelRowsVisibility = function (startIndex) {
            while (scope.rowConfig['row' + startIndex]) {
                scope.rowConfig['row' + startIndex].visible = !scope.rowConfig['row' + startIndex].visible;
                startIndex++;
            }
        }

    }

    static directiveFactory($compile, $http) {
        CyFormRendererDirective.instance = new CyFormRendererDirective($compile, $http);
        return CyFormRendererDirective.instance;
    }
}

CyFormRendererDirective.directiveFactory.$inject = ['$compile', '$http'];

export {CyFormRendererDirective}