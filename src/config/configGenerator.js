/**
 * Created by alexander on 25.10.17.
 */

function generateFormConfig(tableConfig, formAdditionalConfig, isFilter) {
    var formConfig = [];
    tableConfig.forEach(function (configItem) {
        if ((configItem.filter && isFilter) || (configItem.form && !isFilter)) {
            if (configItem.type === "date") {
                if (configItem.form) {
                    formConfig.push(
                        {
                            field: configItem.fields[0],
                            type: configItem.type,
                            label: configItem.formNames[0],
                            classNames: configItem.classNames,
                            row: configItem.row,
                            formId: configItem.formId,
                            formField: configItem.formField,
                            required: isFilter ? false : configItem.required,
                            order: configItem.order
                        }
                    );
                } else {
                    formConfig.push(
                        {
                            field: configItem.fields[0] + "From",
                            type: configItem.type,
                            label: configItem.formNames[0] + " от",
                            classNames: configItem.classNames,
                            row: configItem.row,
                            formId: configItem.formId + "From",
                            formField: configItem.formField + "From",
                            required: isFilter ? false : configItem.required,
                            order: configItem.order
                        }
                    );
                    formConfig.push(
                        {
                            field: configItem.fields[0] + "To",
                            type: configItem.type,
                            label: configItem.formNames[0] + " до",
                            classNames: configItem.classNames,
                            row: configItem.row,
                            formId: configItem.formId + "To",
                            formField: configItem.formField + "To",
                            required: isFilter ? false : configItem.required,
                            order: configItem.order
                        }
                    );
                }
            } else if (configItem.type === "select2") {
                formConfig.push(
                    {
                        field: configItem.fields[0],
                        type: configItem.type,
                        label: configItem.formNames[0],
                        classNames: configItem.classNames,
                        row: configItem.row,
                        formId: configItem.formId,
                        formField: configItem.formField,
                        listProvider: configItem.listProvider,
                        required: isFilter ? false : configItem.required,
                        order: configItem.order
                    }
                );
            }else if (configItem.type === "datetimepicker") {
                formConfig.push(
                    {
                        field: configItem.fields[0],
                        type: configItem.type,
                        label: configItem.formNames[0],
                        classNames: configItem.classNames,
                        row: configItem.row,
                        formId: configItem.formId,
                        formField: configItem.formField,
                        required: isFilter ? false : configItem.required,
                        order: configItem.order
                    }
                );
            } else if (configItem.type === "uiselect") {
                formConfig.push(
                    {
                        fields: configItem.fields,
                        type: configItem.type,
                        multiple: configItem.multiple,
                        tagging: configItem.tagging,
                        label: configItem.formNames[0],
                        classNames: configItem.classNames,
                        row: configItem.row,
                        formId: configItem.formId,
                        formField: configItem.formField,
                        listProvider: configItem.listProvider,
                        required: isFilter ? false : configItem.required,
                        selectChoicesTemplate: configItem.selectChoicesTemplate,
                        selectMatchTemplate: configItem.selectMatchTemplate,
                        order: configItem.order
                    }
                );
            } else if (configItem.type === "uiselectajax") {
                formConfig.push(
                    {
                        fields: configItem.fields,
                        type: configItem.type,
                        multiple: configItem.multiple,
                        label: configItem.formNames[0],
                        classNames: configItem.classNames,
                        row: configItem.row,
                        formId: configItem.formId,
                        formField: configItem.formField,
                        listProvider: configItem.listProvider,
                        required: isFilter ? false : configItem.required,
                        selectChoicesTemplate: configItem.selectChoicesTemplate,
                        selectMatchTemplate: configItem.selectMatchTemplate,
                        ajaxUrl: configItem.ajaxUrl,
                        dataPath: configItem.dataPath,
                        order: configItem.order
                    }
                );
            } else if (configItem.type === "uidatepicker") {
                formConfig.push(
                    {
                        fields: configItem.fields,
                        type: configItem.type,
                        label: configItem.formNames[0],
                        classNames: configItem.classNames,
                        row: configItem.row,
                        formField: configItem.formField,
                        listProvider: configItem.listProvider,
                        required: isFilter ? false : configItemch.required,
                        order: configItem.order
                    }
                );
            } else if (configItem.type === "uidatetimepicker") {
                formConfig.push(
                    {
                        fields: configItem.fields,
                        type: configItem.type,
                        label: configItem.formNames[0],
                        classNames: configItem.classNames,
                        row: configItem.row,
                        formField: configItem.formField,
                        listProvider: configItem.listProvider,
                        required: isFilter ? false : configItem.required,
                        order: configItem.order
                    }
                );
            } else if (configItem.fields.length > 1) {
                configItem.fields.forEach(function (field, index) {
                    formConfig.push(
                        {
                            field: field,
                            type: configItem.type,
                            label: configItem.formNames[index],
                            classNames: configItem.classNames,
                            row: configItem.row,
                            formField: configItem.formField,
                            required: isFilter ? false : configItem.required,
                            order: configItem.order
                        }
                    );
                });
            } else {
                formConfig.push(
                    {
                        field: configItem.fields[0],
                        type: configItem.type,
                        label: configItem.formNames[0],
                        classNames: configItem.classNames,
                        row: configItem.row,
                        formField: configItem.formField,
                        required: isFilter ? false : configItem.required,
                        order: configItem.order
                    }
                );
            }
        }
    });
    return formConfig.concat(formAdditionalConfig).sort(compareConfigItem);
};

// Формируем конфиг для каждой строки с параметром visible. Если параметр не указан, то строка не скрывается
function formRowConfig(config) {
    var rowConfig = {};
    config.forEach(function (configItem) {
        if (configItem.row && !rowConfig['row' + configItem.row]) {
            var rowVisibility = typeof configItem.visible == 'undefined' || configItem.visible == true;
            rowConfig['row' + configItem.row] = {visible: rowVisibility};
        }
    });
    return rowConfig;
}

function configPostprocessor(config) {
    config.forEach(function (configItem) {
        if (configItem.type === "date") {
            $("#" + configItem.formId).datetimepicker();
        } else if (configItem.type === "select2") {
            if (configItem.listProvider.ajax) {
                $("#" + configItem.formId).select2({
                    ajax: configItem.listProvider.ajaxConfig,
                    minimumResultsForSearch: -1
                });
            } else {
                $("#" + configItem.formId).select2({
                    data: configItem.listProvider.data
                });
            }
        }
    });
}

function fromConfigToModel(configItem) {
    if (configItem.type === "select2") {
        var select2Selection = $("#" + configItem.formId).select2("data");
        if (select2Selection.length > 0) {
            return {id: select2Selection[0].id};
        } else {
            return null;
        }
    } else if (configItem.type === "date") {
        var date = $("#" + configItem.formId).data("DateTimePicker").date();
        if (date != null && date != undefined) {
            return date.valueOf();
        } else {
            return null;
        }
    }
    return null;
}

function compareConfigItem(configItem1, configItem2) {
    if (!configItem1 || !configItem2) {
        return 0;
    }
    return configItem1.order - configItem2.order;
}

export {
    generateFormConfig,
    configPostprocessor,
    fromConfigToModel,
    formRowConfig
}
