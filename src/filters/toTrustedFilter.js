toTrusted.$inject = [
    "$sce"
];

export default function toTrusted($sce) {
    return function (text) {
        return $sce.trustAsHtml(text);
    };
}