import {CyFormRendererDirective} from './directives/cy-form-renderer-directive';
import {CyTableRendererDirective} from './directives/cy-table-renderer-directive';
import onScrollEndDirective from './directives/onScrollEndDirective';
import compileDirective from './directives/compileDirective';
import toTrusted from "./filters/toTrustedFilter"

var moduleName = 'cynteka-js';

angular.module(moduleName, [])
    .directive('cyTableRenderer', CyTableRendererDirective.directiveFactory)
    .directive('cyFormRenderer', CyFormRendererDirective.directiveFactory)
    .directive('onScrollEnd', onScrollEndDirective)
    .directive('compile', compileDirective)
    .filter('toTrusted', toTrusted);

export default moduleName;