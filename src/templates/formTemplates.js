/**
 * Created by alexander on 02.11.17.
 */

/*****BEGIN***** Шаблоны для элементов формы *****BEGIN*****/

var stringTemplate = function (label, formModelName, formName, formField, required) {
    var template =
        "<div class=\"form-group\" ng-class=\"{ 'has-error': " + formName + "." + formField + ".$invalid }\">" +
        "<label>" + label + ":</label>" +
        "<input type=\"text\" class=\"form-control\" name=\"" + formField + "\" ng-model=\"" + formModelName + "." + formField + "\" ng-required=\"" + required + "\"/>" +
        "</div>";
    return template;
}

var dateTimePickerTemplate = function (label, formId, formField, formModelName, formName, required) {
    var template =
        "<div class=\"form-group\">" +
        "<label>" + label + ":</label>" +
        "<div class=\"input-group date\" id=\"" + formId + "\">" +
        "<input name=\"" + formField + "\" type=\"date\" class=\"form-control\" ng-model=\"" + formModelName + "." + formField + "\" ng-required=\"" + required + "\"/>" +
        "<span class=\"input-group-addon\">" +
        "<span class=\"glyphicon glyphicon-calendar\"></span>" +
        "</span>" +
        "</div>" +
        "</div>";
        // "<p ng-if=\"!" + formName + "." + formField + ".$valid\">Это поле должно быть заполнено.</p>";
    return template;
}

var select2Template = function (label, formId) {
    var template =
        "<div class=\"form-group\">" +
        "<label>" + label + ":</label>" +
        "<select class=\"form-control\" id=\"" + formId + "\">" +
        "</select>" +
        "</div>";
    return template;
}

var textareaTemplate = function (label, formField, formModelName, formName, required) {
    var template =
        "<div class=\"form-group\" ng-class=\"{ 'has-error': " + formName + "." + formField + ".$invalid }\">" +
        "<label>" + label + ":</label>" +
        "<textarea name=\"" + formField + "\" type=\"text\" class=\"form-control\" ng-model=\"" + formModelName + "." + formField + "\" ng-required=\"" + required + "\"></textarea>" +
        "</div>";
        // "<p ng-if=\"!" + formName + "." + formField + ".$valid\">Это поле должно быть заполнено.</p>";
    return template;
}

var angularDateTimePickerTemplate = function (label, formField, formModelName, formName, required) {
    var template =
        "<div class=\"form-group\" ng-class=\"{ 'has-error': " + formName + "." + formField + ".$invalid }\">" +
        "<label>" + label + ":</label>" +
        "<div class=\"input-group\">" +
        "<input class=\"form-control\" name=\"" + formField + "\" datetimepicker ng-model=\"" + formModelName + "." + formField + "\" ng-required=\"" + required + "\"/>" +
        "<span class=\"input-group-addon\">" +
        "<span class=\"glyphicon glyphicon-calendar\"></span>" +
        "</span>" +
        "</div>" +
        "</div>";
        // "<div class=\"form-group\">" +
        // "<label>" + label + ":</label>" +
        // "<div class=\"input-group\" datetimepicker ng-model=\"" + formModelName + "." + formField + "\" ng-required=\"" + required + "\">" +
        // "<input class=\"form-control\"/>" +
        // "<span class=\"input-group-addon\">" +
        //     "<span class=\"glyphicon glyphicon-calendar\"></span>" +
        // "</span>" +
        // "</div>" +
        // "</div>";
    return template;
}

var uiDatepicker = function (label, formField, formModelName, formName, required) {
    var template =
        "<div class=\"form-group\" ng-class=\"{ 'has-error': " + formName + "." + formField + ".$invalid }\">" +
        "<label>" + label + ":</label>" +
        "<div class=\"input-group\">" +
        "<input type=\"text\" class=\"form-control\" is-open=\"" + formField + ".opened\" name=\"" + formField + "\" uib-datepicker-popup ng-model=\"" + formModelName + "." + formField + "\" class=\"well well-sm\" ng-required=\"" + required + "\"/>" +
        "<span class=\"input-group-btn\">" +
        "<button type=\"button\" class=\"btn btn-default\" ng-click=\"" + formField + ".opened = !" + formField + ".opened\"><i class=\"glyphicon glyphicon-calendar\"></i></button>" +
        "</span>" +
        "</div>" +
        "</div>";
    return template;
}

var uiDatetimepicker = function (label, formField, formModelName, formName, required) {
    var template =
        "<div class=\"form-group\" ng-class=\"{ 'has-error': " + formName + "." + formField + ".$invalid }\">" +
        "<label>" + label + ":</label>" +
        "<div class=\"input-group\">" +
        "<input type=\"text\" class=\"form-control\" datetime-picker name=\"" + formField + "\"  ng-model=\"" + formModelName + "." + formField + "\" is-open=\"" + formField + ".opened\" ng-required=\"" + required + "\"/>" +
        // "<input type=\"text\" class=\"form-control\" is-open=\"" + formField + ".opened\" name=\"" + formField + "\" uib-datepicker-popup ng-model=\"" + formModelName + "." + formField + "\" class=\"well well-sm\" ng-required=\"" + required + "\"/>" +
        "<span class=\"input-group-btn\">" +
        "<button type=\"button\" class=\"btn btn-default\" ng-click=\"" + formField + ".opened = !" + formField + ".opened\"><i class=\"glyphicon glyphicon-calendar\"></i></button>" +
        "</span>" +
        "</div>" +
        "</div>";
    return template;
}

var uiSelectTemplate = function (label, formField, formModelName, formName, index, required, selectChoicesTemplate, selectMatchTemplate, multiple, tagging) {
    var template =
        "<div class=\"form-group\" ng-class=\"{ 'has-error': " + formName + "." + formField + ".$invalid }\">" +
        "<label>" + label + ":</label>" +
        "<ui-select";
        if (tagging) {
            template += " tagging=\"true\" tagging-label=\"\"";
        }
        if (multiple) {
            template += " multiple";
        }
        template += " name=\"" + formField + "\" ng-model=\"" + formModelName + "." + formField + "\" theme=\"bootstrap\" ng-required=\"" + required + "\">" +
        "<ui-select-match allow-clear=\"true\">{{" + selectMatchTemplate + "}}</ui-select-match>" +
        "<ui-select-choices repeat=\"item in config[" + index + "].listProvider.data | filter: $select.search\">" +
        selectChoicesTemplate +
        "</ui-select-choices>" +
        "</ui-select>" +
        "</div>";
    return template;
}

var uiSelectAjaxTemplate = function (label, formField, formModelName, formName, index, required, selectChoicesTemplate, selectMatchTemplate, multiple) {
    var template =
        "<div class=\"form-group\" ng-class=\"{ 'has-error': " + formName + "." + formField + ".$invalid }\">" +
        "<label>" + label + ":</label>" +
        "<ui-select";
        if (multiple) {
            template += " multiple";
        }
        template += " name=\"" + formField + "\" ng-model=\"" + formModelName + "." + formField + "\" theme=\"bootstrap\" ng-required=\"" + required + "\">" +
        "<ui-select-match allow-clear=\"true\">{{" + selectMatchTemplate + "}}</ui-select-match>" +
        "<ui-select-choices refresh=\"uiSelectSearch($select, config[" + index + "].ajaxUrl, config[" + index + "].formField, config[" + index + "].dataPath)\" refresh-delay=\"300\"  repeat=\"item in uiSelectAjaxData_" + formField + " | filter: $select.search\">" +
        selectChoicesTemplate +
        "</ui-select-choices>" +
        "</ui-select>" +
        "</div>";
    return template;
}

var checkBoxTempalte = function (label, formModelName, formName, formField, required) {
    var template =
        "<div class=\"form-check\" ng-class=\"{ 'has-error': " + formName + "." + formField + ".$invalid }\">" +
        "<label class=\"form-check-label\">" +
        "<input type=\"checkbox\" class=\"form-check-input\" name=\"" + formField + "\" ng-model=\"" + formModelName + "." + formField + "\" ng-required=\"" + required + "\">" +
        label +
        "</label>" +
        "</div>";
    return template;
}

/*****END***** Шаблоны для элементов формы *****END*****/

/********** Функция генерации шаблона формы **********/
function generateFormTemplate(config, formName, formModelName, submitFunction, submitButtonText, rowConfig, showFilterButtons) {
    var template =
    "<form ng-submit=\"" + submitFunction + "\" name=\"" + formName + "\">";
    var prevRow = -1;
        template += "<div>";
        config.forEach(function (configItem, cfgIndex) {
            if (prevRow != configItem.row) {
                if (prevRow != -1) {
                    template += "</div>";
                }
                template += "<div class=\"row\" ng-show=\"rowConfig['row" + configItem.row + "'] && rowConfig['row" + configItem.row + "'].visible\">";
            }
            template += "<div class=\"" + configItem.classNames.join(" ") + "\">";
            if (configItem.formRenderer) {
                template += configItem.formRenderer;
            } else {
                if (configItem.type === "string") {
                    template += stringTemplate(configItem.label, formModelName, formName, configItem.formField, configItem.required);
                } else if (configItem.type === "date") {
                    template += dateTimePickerTemplate(configItem.label, configItem.formId, configItem.formField, formModelName, formName, configItem.required);
                } else if (configItem.type === "select2") {
                    template += select2Template(configItem.label, configItem.formId);
                } else if (configItem.type === "textarea") {
                    template += textareaTemplate(configItem.label, configItem.formField, formModelName, formName, configItem.required);
                } else if (configItem.type === "datetimepicker") {
                    template += angularDateTimePickerTemplate(configItem.label, configItem.formField, formModelName, formName, configItem.required);
                } else if (configItem.type === "uiselect") {
                    template += uiSelectTemplate(configItem.label, configItem.formField, formModelName, formName, cfgIndex, configItem.required, configItem.selectChoicesTemplate, configItem.selectMatchTemplate, configItem.multiple, configItem.tagging);
                } else if (configItem.type === "uiselectajax") {
                    template += uiSelectAjaxTemplate(configItem.label, configItem.formField, formModelName, formName, cfgIndex, configItem.required, configItem.selectChoicesTemplate, configItem.selectMatchTemplate, configItem.multiple);
                } else if (configItem.type === "uidatepicker") {
                    template += uiDatepicker(configItem.label, configItem.formField, formModelName, formName, configItem.required);
                } else if (configItem.type === "uidatetimepicker") {
                    template += uiDatetimepicker(configItem.label, configItem.formField, formModelName, formName, configItem.required);
                } else if (configItem.type === "checkbox") {
                    template += checkBoxTempalte(configItem.label, formModelName, formName, configItem.formField, configItem.required)
                }
            }
            template += "</div>";
            prevRow = configItem.row;
        });
        template += "</div>";
        template += "<div class=\"btn-group filter-buttons\" ng-show=\"" + showFilterButtons + "\">";
        template += "<button class=\"btn btn-success\" type=\"submit\" ng-disabled=\"" + formName + ".$invalid\">" + submitButtonText + "</button>";
        template += "<button class=\"btn btn\" type=\"button\" ng-click=\"clear()\">Очистить</button>";
        template += "</div>";
    template +=
    "</form>";
    return template;
}

/********** Функция генерации шаблона таблицы **********/
/*function generateTableTemplate(config, data, scope) {
    var template =
    "<table class=\"table table-bordered\">" +
        "<tr>" +
            "<th ng-repeat=\"th in config\" ng-click=\"order(th.orderField)\">{{th.title}}</th>" +
        "</tr>" +
        "<tr ng-repeat=\"tr in data\">";
    template +=
            "<td ng-repeat=\"td in config\">" +
                "<div ng-if=\"!isTemplate(td.renderer)\" ng-bind-html=\"td.renderer(tr, td) | toTrusted\"></div>" +
                "<div ng-if=\"isTemplate(td.renderer)\" ng-include=\"td.renderer\"></div>" +
            "</td>";
    template +=
        "</tr>" +
    "</table>";
    template +=
        "<div class=\"text-center\">" +
        "<button type=\"button\" class=\"btn btn-success\" ng-click=\"loadMore()\">Загрузить еще</button>" +
        "</div>";
    return template;
}*/

export {stringTemplate, dateTimePickerTemplate, select2Template, textareaTemplate, angularDateTimePickerTemplate, uiSelectTemplate, generateFormTemplate/*, generateTableTemplate*/}